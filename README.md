# wd-component-circular-progressbar

**wd-component-circular-progressbar** can be used to create circular progressbar

  - LESS mixin for customization

### Version
0.0.1

### Installation

Using [Bower](http://bower.io/):

```sh
$ bower install git@bitbucket.org:webdevcomponents/wd-component-circular-progressbar.git
```

Using Git
```sh
$ git clone git@bitbucket.org:webdevcomponents/wd-component-circular-progressbar.git
```


Add module as dependency
```
#!javascript

angular.module("app", [
	'web-dev.components.circular-progress'
]);
```

### Customization
LESS mixin **.wd-circular-progress()** is available in **src/web-dev.component.circular-progress.less**
```
#!less

@import "../../bower_components/wd-component-circular-progressbar/src/web-dev.component.circular-progress.less";
```

### Using 

```
#!less


wd-circular-progress {
  .wd-circular-progress([options]) 
}
```

Options

* **size: 100px**
width/height of element
* **progressbar-width: 4px**
width of progressbar
* **progressbar-color: #000**
width of progressbar
* **inset-color: #fff**
background color of inner part ***Important:*** that color can't use tranparency
* **border-color: transparent** 
color of non-active part of progressbar
* **font-size: 22px**
font-size of inner text
* **color: selection-color**
font color of inner text
* **transition-duration: 1s**
duration of progress transition

### Examples
1.

```
#!less

wd-circular-progress {
  .wd-circular-progress();
}
```
 
![default.jpg](https://bitbucket.org/repo/dEBpp8/images/1060611753-default.jpg)

2.

```
#!less

.wd-circular-progress--big {
  .wd-circular-progress(200px, 8px, #106cc8, #00b0ff, #80d8ff, 50px, #fff, 0s);
}
```
 
![default1.jpg](https://bitbucket.org/repo/dEBpp8/images/3922397078-default1.jpg)


3.

```
#!less

.wd-circular-progress--small {
  .wd-circular-progress(64px, 3px, #E64A19, #fff, #FBE9E7, 18px, #E64A19, .5s);
}
```

![default2.jpg](https://bitbucket.org/repo/dEBpp8/images/2378796581-default2.jpg)


### Todos

 - Check browser compability
 - Add examples

License
----
MIT
var gulp 		= require('gulp'),
	babel		= require('gulp-babel'),
	less		= require('gulp-less'),
	minifyCSS	= require('gulp-cssmin'),
	rename		= require('gulp-rename'),
	uglify		= require('gulp-uglify');

gulp.task('js', function(){
	return gulp.src('src/web-dev.component.circular-progress.es6')
		.pipe(babel())
		.pipe(rename({extname: '.js'}))
		.pipe(gulp.dest('dist'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('dist'))
});

gulp.task('css', function(){
	return gulp.src('src/web-dev.component.circular-progress.default.less')
		.pipe(less())
		.pipe(gulp.dest('dist'))
		.pipe(minifyCSS())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('dist'))
});

gulp.task('default', ['css', 'js']);
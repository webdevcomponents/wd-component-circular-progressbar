(function() {
	angular
		.module('web-dev.components.circular-progress', []) 
		.directive('wdCircularProgress', wdCircularProgress)
		.run(run)

	function wdCircularProgress() {
		return {
			bindToController: {
				value: '@'
			},
			controller: controller,
			controllerAs: 'wdCircularProgress',
			link: link, 
			templateUrl: 'partials/wdCircularProgress.html',
			restrict: 'E',
			scope: true,
			transclude: true,
		}
	}

	function link(scope, element) {
		element.attr('role', 'progressbar');
		element.attr('aria-valuemin', 0);
		element.attr('aria-valuemax', 100);
	}

	function controller($scope) {
		$scope.$watch( () => this.value, (value) => {
			this.value = value = clamp(value);
			this.mask1 = createMask(1.8 * value)
			this.mask2 = createMask(3.6 * value)
		})

		function createMask(angle) {
			return {
				'-webkit-transform': `rotate(${angle}deg)`,
				'-moz-transform': `rotate(${angle}deg)`,
				'transform': `rotate(${angle}deg)`
			}
		}
		
		function clamp(value) {
			return Math.max(0, Math.min(value || 0, 100));
		}
	}

	function run($templateCache) {
		var template = `
			<div class="wd-circular-progress__circle">
				<div class="wd-circular-progress__mask" ng-style="wdCircularProgress.mask1">
					<div class="wd-circular-progress__fill" ng-style="wdCircularProgress.mask1"></div>
				</div>
				<div class="wd-circular-progress__mask">
					<div class="wd-circular-progress__fill" ng-style="wdCircularProgress.mask1"></div>
					<div class="wd-circular-progress__fill" ng-style="wdCircularProgress.mask2"></div>
				</div>
				<div class="wd-circular-progress__shadow"></div>
			</div>
			<div class="wd-circular-progress__inset">{{wdCircularProgress.value}}%</div>
		`; 
		$templateCache.put('partials/wdCircularProgress.html', template);
	}
})();
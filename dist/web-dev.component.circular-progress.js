'use strict';

(function () {
	angular.module('web-dev.components.circular-progress', []).directive('wdCircularProgress', wdCircularProgress).run(run);

	function wdCircularProgress() {
		return {
			bindToController: {
				value: '@'
			},
			controller: controller,
			controllerAs: 'wdCircularProgress',
			link: link,
			templateUrl: 'partials/wdCircularProgress.html',
			restrict: 'E',
			scope: true,
			transclude: true
		};
	}

	function link(scope, element) {
		element.attr('role', 'progressbar');
		element.attr('aria-valuemin', 0);
		element.attr('aria-valuemax', 100);
	}

	function controller($scope) {
		var _this = this;

		$scope.$watch(function () {
			return _this.value;
		}, function (value) {
			_this.value = value = clamp(value);
			_this.mask1 = createMask(1.8 * value);
			_this.mask2 = createMask(3.6 * value);
		});

		function createMask(angle) {
			return {
				'-webkit-transform': 'rotate(' + angle + 'deg)',
				'-moz-transform': 'rotate(' + angle + 'deg)',
				'transform': 'rotate(' + angle + 'deg)'
			};
		}

		function clamp(value) {
			return Math.max(0, Math.min(value || 0, 100));
		}
	}

	function run($templateCache) {
		var template = '\n\t\t\t<div class="wd-circular-progress__circle">\n\t\t\t\t<div class="wd-circular-progress__mask" ng-style="wdCircularProgress.mask1">\n\t\t\t\t\t<div class="wd-circular-progress__fill" ng-style="wdCircularProgress.mask1"></div>\n\t\t\t\t</div>\n\t\t\t\t<div class="wd-circular-progress__mask">\n\t\t\t\t\t<div class="wd-circular-progress__fill" ng-style="wdCircularProgress.mask1"></div>\n\t\t\t\t\t<div class="wd-circular-progress__fill" ng-style="wdCircularProgress.mask2"></div>\n\t\t\t\t</div>\n\t\t\t\t<div class="wd-circular-progress__shadow"></div>\n\t\t\t</div>\n\t\t\t<div class="wd-circular-progress__inset">{{wdCircularProgress.value}}%</div>\n\t\t';
		$templateCache.put('partials/wdCircularProgress.html', template);
	}
})();